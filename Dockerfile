from ubuntu as builder
run apt-get update && apt-get install -y wget unzip && rm -rf /var/lib/apt/lists/*

workdir /tmp

arg version=2.2.1
run wget https://bitbucket.org/kokonech/qualimap/downloads/qualimap_v${version}.zip \
 && unzip qualimap_v${version}.zip \
 && mv qualimap_v${version} qualimap

from openjdk:6-jre
copy --from=builder /tmp/qualimap /qualimap
entrypoint /qualimap/qualimap
